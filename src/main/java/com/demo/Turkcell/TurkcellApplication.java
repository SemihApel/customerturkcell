package com.demo.Turkcell;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
		//(scanBasePackages = {"com.demo.Turkcell.Services","com.demo.Turkcell.Controller"})


public class TurkcellApplication {

	public static void main(String[] args) {
		SpringApplication.run(TurkcellApplication.class, args);
	}

}
