package com.demo.Turkcell.Services;

import com.demo.Turkcell.Dto.MobilePackDto;
import com.demo.Turkcell.Model.MobilePack;
import com.demo.Turkcell.Repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public interface MobilePackServices {

    public List<MobilePackDto> getAllMobilePack();
    public MobilePackDto getMobilePackById(long id);
    public  boolean addMobilePack(MobilePackDto mobilePackDto);
    public boolean deleteMobilePackById(long id);
    public boolean updateMobilePack(MobilePackDto mobilePackDto);



}
