package com.demo.Turkcell.Services;

import com.demo.Turkcell.Dto.CustomerDto;
import com.demo.Turkcell.Model.Customer;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public interface CustomerServices  {
    public List<CustomerDto> getAllList();
    public  CustomerDto getCustomerById(long id);
    public   boolean addCustomer(CustomerDto customer);
    public boolean deleteCustomer(long id);
    public boolean updateCustomer(CustomerDto customer);
    public CustomerDto getCustomerByGsm(long gsm);

}
