package com.demo.Turkcell.Controller;

import com.demo.Turkcell.Dto.CustomerDto;
import com.demo.Turkcell.Dto.MobilePackDto;
import com.demo.Turkcell.Mapper.MobilePackMapper;
import com.demo.Turkcell.Model.MobilePack;
import com.demo.Turkcell.Services.CustomerServices;
import com.demo.Turkcell.Services.MobilePackServices;
import lombok.AllArgsConstructor;
import org.dom4j.rule.Mode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLOutput;
import java.util.List;

@RestController
@AllArgsConstructor
@CrossOrigin(origins = "http://localhost:4200")

public class MobilePackController {

     private final MobilePackServices mobilePackServices;
     public  final HttpServletRequest request;
     private final CustomerServices customerServices;
     private  final MobilePackMapper mobilePackMapper;
    // @RequestMapping(value = "/" , method = RequestMethod.GET)

    @PostMapping(value = "/package")
    public ModelAndView getAllMobilePack(String gsm){
        ModelAndView modelAndView =new ModelAndView("package");
        List<MobilePackDto>  mobilePackDtos = mobilePackServices.getAllMobilePack();
        //System.out.println("size of list"+ mobilePackDtos.size());

        modelAndView.addObject("packs", mobilePackDtos);
        modelAndView.addObject("gsm", gsm);
        //modelAndView.addObject("customerPackId",customerPackId);
        request.getSession().setAttribute("customerGsm",gsm);
        CustomerDto customerDto =customerServices.getCustomerByGsm(Long.parseLong(gsm));
        request.getSession().setAttribute("customerPackId",customerDto.getMobilePack().getId());

        return modelAndView;


    }

    @RequestMapping(value = "/getdetail/{id}",method = RequestMethod.GET)
    public ModelAndView getMobilePackDetail(@PathVariable(value = "id") long id){
        ModelAndView modelAndView =new ModelAndView("show");
        MobilePackDto mobilePackDtos =mobilePackServices.getMobilePackById(id);
        request.getSession().setAttribute("selectedPack",id);
      //  System.out.println(" Mobile pack descriptions :  "+ mobilePackDtos.getDescriptions());
        modelAndView.addObject("pack", mobilePackDtos);
        return  modelAndView;

    }


    @RequestMapping(value = "/packages", method = RequestMethod.GET)
    public ResponseEntity<List<MobilePackDto>> getAllMobilePack2(){
     //   ModelAndView modelAndView =new ModelAndView("package");
        List<MobilePackDto>  mobilePackDtos = mobilePackServices.getAllMobilePack();
        //System.out.println("size of list"+ mobilePackDtos.size());
        // System.out.println("mobile pack method calıstı");
       // modelAndView.addObject("packs", mobilePackDtos);

        return new ResponseEntity(mobilePackDtos, HttpStatus.OK);


    }


    @RequestMapping(value = "/get/{id}",method = RequestMethod.GET)
    public ResponseEntity<MobilePackDto> getMobilePackById2(@PathVariable("id") long id ){

         MobilePackDto pack = mobilePackServices.getMobilePackById(id);

        return new ResponseEntity<>(pack,HttpStatus.OK);

    }

    @RequestMapping(value ="/change/{id}", method = RequestMethod.POST)
    public ModelAndView changepackage(@PathVariable(value = "id") Long id){
        ModelAndView modelAndView =new ModelAndView("update");
        // long customerGsm =  Long.parseLong(request.getParameter("gsm"));
        long customerGsm=Long.parseLong(request.getSession().getAttribute("customerGsm").toString());
      //  long customerGsm=5512434581L;
        System.out.println(customerGsm+" customer Gsm ");
        MobilePackDto mobilePack=mobilePackServices.getMobilePackById(id);
        CustomerDto customer = customerServices.getCustomerByGsm(customerGsm);
        MobilePack mobilePack1=mobilePackMapper.dtoToEntiy(mobilePack);
        customer.setMobilePack(mobilePack1);
        customerServices.updateCustomer(customer);
        System.out.println("update is done ");

        return modelAndView;


    }



}
