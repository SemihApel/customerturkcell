package com.demo.Turkcell.Controller;

import com.demo.Turkcell.Dto.CustomerDto;
import com.demo.Turkcell.Dto.MobilePackDto;
import com.demo.Turkcell.Mapper.MobilePackMapper;
import com.demo.Turkcell.Model.Customer;
import com.demo.Turkcell.Model.MobilePack;
import com.demo.Turkcell.Repository.CustomerRepository;
import com.demo.Turkcell.Services.CustomerServices;
import com.demo.Turkcell.Services.MobilePackServices;
import com.demo.Turkcell.ServicesImp.CustomerServicesImp;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Controller
@RequestMapping("/customer")
@AllArgsConstructor
public class CustomerController {



    private final CustomerServicesImp customerServices;
    private final HttpServletRequest request;
    private final MobilePackServices mobilePackServices;
    private final MobilePackMapper mobilePackMapper;

    @RequestMapping(value="/getall",method= RequestMethod.GET)
    public ModelAndView getAllCustomer(){
        ModelAndView modelAndView =new ModelAndView("customerList");
        List<CustomerDto> customerDtos =customerServices.getAllList();
        modelAndView.addObject("customerList",customerDtos);
        return  modelAndView;

    }
    @RequestMapping(value ="/change/{id}", method = RequestMethod.POST)
    public ModelAndView changepackage(@RequestParam(value = "id") long id){
        ModelAndView modelAndView =new ModelAndView("update");
        // long customerGsm =  Long.parseLong(request.getParameter("gsm")); mehmet semih apel
         long customerGsm=Long.parseLong(request.getSession().getAttribute("customerGsm").toString());
         System.out.println(customerGsm+" customer Gsm ");
          MobilePackDto mobilePack=mobilePackServices.getMobilePackById(id);
          CustomerDto customer = customerServices.getCustomerByGsm(customerGsm);
          MobilePack mobilePack1=mobilePackMapper.dtoToEntiy(mobilePack);
          customer.setMobilePack(mobilePack1);
          customerServices.updateCustomer(customer);
          System.out.println("update is done ");

        return modelAndView;


    }




//////old method
    @RequestMapping(value="/get",method = RequestMethod.GET)
    public ResponseEntity<List<Customer>> getAllCustomer2(){
        List<CustomerDto> customers=new ArrayList<>();
        customerServices.getAllList().forEach(a-> customers.add(a));
        return new ResponseEntity(customers, HttpStatus.OK);
    }

//    @RequestMapping(value="/savecustomer",method=RequestMethod.PUT)
    public ResponseEntity<Customer> saveCustomer(@RequestBody CustomerDto customer){
        try {
            customerServices.addCustomer(customer);
            System.out.print("...Kayıt Oluşturuldu");
        }catch(Exception e) {
            System.out.println(e.getMessage());
            System.out.println("....Kayıt Oluşturulamadı...");

        }
        return new ResponseEntity(customer,HttpStatus.CREATED);
    }







}
