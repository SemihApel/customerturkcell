package com.demo.Turkcell.Mapper;

import com.demo.Turkcell.Dto.MobilePackDto;
import com.demo.Turkcell.Model.MobilePack;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MobilePackMapper {

        MobilePack dtoToEntiy(MobilePackDto mobilePackDto);
        MobilePackDto entityToDto(MobilePack mobilePack);

        List<MobilePack> dtoListToDtoList(List<MobilePackDto> mobilePackDtos);
        List<MobilePackDto> entityListToDtoList(List<MobilePack> mobilePacks);



}
