package com.demo.Turkcell.Mapper;

import com.demo.Turkcell.Dto.CustomerDto;
import com.demo.Turkcell.Dto.MobilePackDto;
import com.demo.Turkcell.Model.Customer;
import com.demo.Turkcell.Model.MobilePack;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring",uses = MobilePack.class)
public interface CustomerMapper {
  Customer dtoToEntity(CustomerDto customerDto);
  CustomerDto  entityToDto(Customer customer);
  List<Customer> dtoListToDtoList(List<CustomerDto> customerDtos);
  List<CustomerDto> entityListToDtoList(List<Customer> customers);
}
