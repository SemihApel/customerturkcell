package com.demo.Turkcell.Repository;

import com.demo.Turkcell.Model.Customer;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer,Long> {

    Customer getCustomerByGsm(long gsm);
}
