package com.demo.Turkcell.Repository;

import com.demo.Turkcell.Model.MobilePack;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface MobilePackRepository extends JpaRepository< MobilePack,Long> {

}
