package com.demo.Turkcell.Dto;

import com.demo.Turkcell.Model.Customer;
import lombok.Data;
import lombok.NonNull;

import javax.persistence.Column;
import java.util.ArrayList;
import java.util.List;

@Data
public class MobilePackDto {
    long id;
    String name;
    int phoneMinute;
    int sms;
    double net;
    String descriptions;
    List<CustomerDto> customer=new ArrayList<>() ;
}
