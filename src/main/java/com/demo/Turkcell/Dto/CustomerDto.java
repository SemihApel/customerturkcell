package com.demo.Turkcell.Dto;

import com.demo.Turkcell.Model.MobilePack;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;

@Data

public class CustomerDto {


    private  long id;
    private  long gsm;
    private  String name;
    private  String surname;

    private  MobilePack mobilePack;


}
