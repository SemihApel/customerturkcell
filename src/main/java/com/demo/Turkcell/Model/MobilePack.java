package com.demo.Turkcell.Model;

import lombok.*;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
@Component
@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Table
@Entity
public class MobilePack {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;
    @Column
    @NonNull
    String name;
    @Column
    @NonNull
    int phoneMinute;
    @Column
    @NonNull
    int sms;
    @Column
    @NonNull
    double net;
    @Column(length = 30000)
    @NonNull
    String descriptions;
    @OneToMany(
            mappedBy="mobilePack",
            cascade=CascadeType.ALL,
            orphanRemoval=true,
            fetch=FetchType.LAZY,
            targetEntity=Customer.class

    )
    List<Customer> customers =new ArrayList<>();



}
