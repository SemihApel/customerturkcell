package com.demo.Turkcell.Model;


import lombok.*;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table
public class Customer {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
     private  long id;
     private  long gsm;
     private  String name;
     private  String surname;

     @ManyToOne
     @JoinColumn
     private  MobilePack mobilePack;



}
