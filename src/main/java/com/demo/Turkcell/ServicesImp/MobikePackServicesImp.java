package com.demo.Turkcell.ServicesImp;

import com.demo.Turkcell.Dto.MobilePackDto;
import com.demo.Turkcell.Mapper.MobilePackMapper;
import com.demo.Turkcell.Model.MobilePack;
import com.demo.Turkcell.Repository.MobilePackRepository;
import com.demo.Turkcell.Services.MobilePackServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class MobikePackServicesImp implements MobilePackServices {
    @Autowired
    MobilePackRepository mobilePackRepository;
    @Autowired
    MobilePackMapper mobilePackMapper;
    @Override
    public List<MobilePackDto> getAllMobilePack() {
        List<MobilePack> mobilePacks =new ArrayList<>();
        mobilePackRepository.findAll().forEach(a-> mobilePacks.add(a));
        List<MobilePackDto> mobilePackDtos =new ArrayList<>();
        mobilePackDtos=mobilePackMapper.entityListToDtoList(mobilePacks);
        return mobilePackDtos;
    }

    @Override
    public MobilePackDto getMobilePackById(long id) {
        MobilePack mP =mobilePackRepository.findById(id).get();
       MobilePackDto mobilePackDto=mobilePackMapper.entityToDto(mP);
        return  mobilePackDto;
    }

    @Override
    public boolean addMobilePack(MobilePackDto mobilePackDto) {
        try{
            MobilePack mobilePack =mobilePackMapper.dtoToEntiy(mobilePackDto);
            mobilePackRepository.save(mobilePack);
            System.out.println("Kayot oluşturuldu");
            return true;

        }catch(Exception e){
            System.out.println(e.getMessage());
            System.out.println("kayıt oluşturulamadı");
            return false;
        }

    }

    @Override
    public boolean deleteMobilePackById(long id) {
        try{
            mobilePackRepository.deleteById(id);
            System.out.println("Kayıt Silindi");
            return true;
        }catch(Exception e){
            System.out.println(e.getMessage());
            return false;
        }

    }

    @Override
    public boolean updateMobilePack(MobilePackDto mobilePackDto) {
        try{
            MobilePack mobilePack=mobilePackMapper.dtoToEntiy(mobilePackDto);
            mobilePackRepository.save(mobilePack);
            System.out.println("Kayıt güncellendi");

            return true;
        }catch (Exception e){
            System.out.println("kayıt güncellenemedi");
            return false;
        }
    }
}
