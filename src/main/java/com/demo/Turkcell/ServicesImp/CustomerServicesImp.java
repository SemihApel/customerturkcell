package com.demo.Turkcell.ServicesImp;

import com.demo.Turkcell.Dto.CustomerDto;
import com.demo.Turkcell.Mapper.CustomerMapper;
import com.demo.Turkcell.Mapper.MobilePackMapper;
import com.demo.Turkcell.Model.Customer;
import com.demo.Turkcell.Repository.CustomerRepository;
import com.demo.Turkcell.Services.CustomerServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;
@Service
@Transactional
public class CustomerServicesImp implements CustomerServices {

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    CustomerMapper customerMapper;
    @Override
    public List<CustomerDto> getAllList()  {
    List<Customer> customers =new ArrayList<>();
    customerRepository.findAll().forEach(aLong -> customers.add(aLong));
    List<CustomerDto> customersDto =new ArrayList<>();
    customersDto= customerMapper.entityListToDtoList(customers);
        return  customersDto;
    }

    @Override
    public CustomerDto getCustomerById(long id) {
        Customer c;
       c =customerRepository.findById(id).get();
       CustomerDto cDto=customerMapper.entityToDto(c);
        return  cDto;
    }

    @Override
    public boolean addCustomer(CustomerDto customerDto) {

        try{
           Customer customer=customerMapper.dtoToEntity(customerDto);
            customerRepository.save(customer);
            System.out.println("Kayıt oluşturuldu");
            return true;
        }catch (Exception e){
            System.out.println("Kayıt Oluşturulamadı ");
            System.out.println(e);
            return false;
        }
    }

    @Override
    public boolean deleteCustomer(long id) {

        try{
            customerRepository.deleteById(id);
            System.out.println("Kayıt Silindi");
            return true;
        }catch (Exception e){
            System.out.println("Kayıt silinemedi");
            return false;
        }
    }

    @Override
    public boolean updateCustomer(CustomerDto customerDto)  {

        try{
            Customer customer=customerMapper.dtoToEntity(customerDto);
            customerRepository.save(customer);
            System.out.println("kayıt güncellendi");
            return true;
        }catch(Exception e){
            System.out.println("Kayıt Güncellenemedi");
            return false;
        }

    }

    @Override
    public CustomerDto getCustomerByGsm(long gsm) {
        Customer c;
        c =customerRepository.getCustomerByGsm(gsm);
        CustomerDto cDto=customerMapper.entityToDto(c);
        return  cDto;
    }
}

