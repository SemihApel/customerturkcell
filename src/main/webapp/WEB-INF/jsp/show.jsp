<%@ page import="java.util.Scanner" %><%--
  Created by IntelliJ IDEA.
  User: mehmetsemihapel
  Date: 11.08.2021
  Time: 11:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<head>
    <title>Title</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">


    <style>
        .center{
            margin: auto;
            margin-top: 40px;
            width: 40%;
            padding: 15px;

        }
    </style>
</head>


<body>
<form id="form" action="/change/${pack.id}" method="post">
<div class="center">
    <div class="col">
        <div class="card shadow-sm">
            <svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Thumbnail" preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title><rect width="100%" height="100%" fill="#55595c"/><text x="50%" y="50%" fill="#eceeef" dy=".3em">${pack.name}</text></svg>

            <div class="card-body">
                <p class="card-text">
                <td>${pack.descriptions}</td><br>
                NET <td>${pack.net}</td><br>
                Phone <td>${pack.phoneMinute}</td><br>
                Sms   <td>${pack.sms}</td><br>
                </p>
                <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                        <button type="submit" class="btn btn-sm btn-outline-secondary"> Get this Package</button>

                    </div>
                    <small class="text-muted">Turkcell’le bağlan hayata</small>
                </div>
            </div>
        </div>
    </div>
</div>
</form>


</div>

<script src="/docs/5.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>

</body>
</html>
