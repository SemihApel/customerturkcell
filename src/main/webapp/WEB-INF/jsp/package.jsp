<%--
  Created by IntelliJ IDEA.
  User: mehmetsemihapel
  Date: 23.08.2021
  Time: 10:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.87.0">
    <title>Album example · Bootstrap v5.1</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.1/examples/album/">



    <!-- Bootstrap core CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="/docs/5.1/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
    <link rel="icon" href="/docs/5.1/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="/docs/5.1/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
    <link rel="manifest" href="/docs/5.1/assets/img/favicons/manifest.json">
    <link rel="mask-icon" href="/docs/5.1/assets/img/favicons/safari-pinned-tab.svg" color="#7952b3">
    <link rel="icon" href="/docs/5.1/assets/img/favicons/favicon.ico">
    <meta name="theme-color" content="#7952b3">


    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>


</head>
<body>

<main>
    <section class="py-5 text-center container">
        <div class="row py-lg-5">
            <div class="col-lg-6 col-md-8 mx-auto">
                <h1 class="fw-light">Turkcell </h1>
                <p class="lead text-muted">Get Your Local SIM Card from Turkey’s Leader Operator
                    You can get your SIM card from the nearest Turkcell store.</p>
                <p class="lead text-muted">
                    Your GSM :    ${gsm}<br>
               Your Avaliable Mobile Pack Id :     ${customerPackId}

                </p>
            </div>
        </div>
    </section>

<div class="album py-5 bg-light">
    <div class="container">

        <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
            <c:forEach items="${packs}" var="element">
                <c:if test="${element.id==customerPackId}">

                </c:if>

            <div class="col">
                <div class="card shadow-sm">
                    <svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Thumbnail" preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title><rect width="100%" height="100%" fill="#55595c"/><text x="50%" y="50%" fill="#eceeef" dy=".3em">${element.name}</text></svg>

                    <div class="card-body">
                        <p class="card-text">
                        NET <td>${element.net}</td><br>
                        Phone <td>${element.phoneMinute}</td><br>
                        Sms   <td>${element.sms}</td><br>
                        </p>
                        <div class="d-flex justify-content-between align-items-center">
                            <c:if test="${element.id!=customerPackId}">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-outline-secondary">  <a href="/getdetail/${element.id}">Detail</a></button>
                                </div>
                            </c:if>
                            <c:if test="${element.id==customerPackId}">
                                <div class="btn-group"> <p style="">Yours Package!!</p></button>
                                </div>
                            </c:if>


                            <small class="text-muted">Turkcell’le bağlan hayata</small>
                        </div>
                    </div>
                </div>
            </div>
                </c:forEach>








        </div>
    </div>
</div>

</main>
</div>

<script src="/docs/5.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>


</body>
</html>